PHP
=========

En rolle som setter opp php 7.4 (fra application stream) på instans som Default/Single installation, basert på veiledning fra https://rpms.remirepo.net/wizard/.
Rollen legger også til php-fpm-apache integrasjon, ved å legge inn en ekstra konfigurasjonsfil for apache i `conf.d` mappen.

Det er tagger for kjøring av rollen mot centos  som er EOL i juni 2024.

Det er anbefalt å ikke bruke ustøttede versjoner av php, som 74 eller tidligere, men pakkene i Remi er ganske hyppig patchet sammenlignet med RHEL sine pakker i hovedrepo.

Denne rollen er i aktiv utvikling for å kjøre for EL8 servere.

For øyeblikket er kun installasjon via application-stream for 7.4 prøvd, som finnes i Full Life Application Streams Release Life Cycle for EL8. 
Se mer i https://access.redhat.com/support/policy/updates/rhel-app-streams-life-cycle. 
modulen for php er ikke hyppig oppdatert, men vil komme med oppdateringer ut levetiden til EL8.
Policy for oppdatering ser lik ut som for pakker i hoved-rpm som er i sine siste 5 år av levetiden.

>  During the Full Support Phase, Red Hat defined Critical and Important Security errata advisories (RHSAs) and Urgent and Selected (at Red Hat discretion) High Priority Bug Fix errata advisories (RHBAs) may be released as they become available. Other errata advisories may be delivered as appropriate.

Requirements
------------
Avhengig av at apache er installert. Rollen legger til en apache konfigurasjon for å koble php-fpm som er installert med apache.

`remi` benytter apache bruker som kjører php-fpm rolle.

Role Variables
--------------

# different php_version for remi vs system packages, use 8.0 etc when installing from application stream
php_version: "80" # 54/55/56/70/71/72/73/74/80/81/82, se tilgjengelige alternativ under remi repo.
php_packages_extras: ["php-pdo","php-xml"] # sett pakker som skal installeres, pakker kan browses fra https://rpms.remirepo.net/enterprise/7/
php_composer: false # false/true, installerer pakke fra remi.
php_composer_update: true # force update of composer from getcomposer, doesn't update by default
php_writable_paths: #setter sefcontext for at php skal få lov til å skrive i paths. gir også write permission til user.
- "/var/www/html/cache"
- "/var/www/html/upload"

php_directorymatch_disable_regex: "^/var/www/run-php-here/but-ignore-this-folder|/var/www/run-php-here/ignore-also-this)" # not defined by default

php_directorymatch_enable_regex: "^(/var/www/run-php-here|/opt/www/run-php-also-here)/.*$"  # default set to `/var/www/html/.*$`

php_ini_options: 
- name: "memory_limit" 
  value: "175M"
  section:"PHP"
- name: "..."
  value: "..."
  section: "CLI Server"  # ingen poeng i å definere per nå.er uten betydning siden de ikke blir brukt av php versjoner, og regnes som optional. ini navn blir brukt                         # for å identifisere verdi, og g�r at konfigurasjon for moduler etc, har unike navn.

Dependencies
------------
Enterprise linux version 8 (Kun Rocky Linux i bruk)

httpd

Example Playbook
----------------

```
- name: "Eksempel playbook som konfigurer alt som kan konfigureres"
  hosts: "termwiki_{{ deploy_env }}"
  remote_user: centos
  tasks:
   - import_role:
       name: el8

   - import_role:
       name: "httpd"

   - import_role:
        name: "php"
      vars:
        php_version: "81" # kan også sette tidligere utgaver som er i remi-repo
	php_packages_extra ["php-pdo", "php_xml"]
        php_selinux: false
        php_ini_options:
        # for Replace module for mediawiki
        - name: "max_input_vars"
          value: "10000"
        php_composer: true
        php_writable_paths:
        - "/var/www/html/cache"
      become: true
```


```
- name: "Eksempel for en minimumsplaybook"
  hosts: "termwiki_{{ deploy_env }}"
  remote_user: centos
  tasks:
  - name: "importer httpd rolle, php rollen avhenger av å ha en apache installert"
    import_role:
      name: "httpd"
      ...

  - name: "minimum rolle for å kjøre php, bruker default php_version (80)"
    import_role:
      name: php
```

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
